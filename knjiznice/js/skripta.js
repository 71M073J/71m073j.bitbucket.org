/* global L global distance*/
var koordinate = [];
var mapa;
var markerji = [];
var poligoni = [];
var multipoligoni = [];
var lineStringi = [];
const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;
window.addEventListener('load', function () {
  // Osnovne lastnosti mape    

  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 15
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  dodajBolnisniceNaZemljevid();
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    obarvajNajbliznjo(latlng);
  }

  mapa.on('click', obKlikuNaMapo);
  
});
function obarvajNajbliznjo(latlng){
      var min = 123456789;
      var minIndex;
      var minIndexj;
      for(var i in koordinate){//za vsak zapis objekta in njegove zap.št
        for(var j in koordinate[i][0]){//za vse pare koordinat
          //console.log(i + " holy hell " + j);
          var dist = distance(latlng.lat, latlng.lng, koordinate[i][0][j][0], koordinate[i][0][j][1], "K");
          if(dist<min){
            min = dist;
            minIndex = koordinate[i][1];//katera stavba po vrsti je
            minIndexj = j;
          }
        }
      }
      console.log(minIndex);
      pridobiPodatke(function(bolnisnice){
        console.log(bolnisnice.features[minIndex].geometry.type);
        
        
        
        
          switch(bolnisnice.features[minIndex].geometry.type){
            case "Polygon":
              if(bolnisnice.features[minIndex].geometry.coordinates.length > 1){//multipolygon
                for(var j in multipoligoni){
                  for(var k in multipoligoni[j]){
                    mapa.removeLayer(multipoligoni[j][k]);
                    //console.log("removed a multipoly");
                  }
                }
                for(var j in poligoni){
                  mapa.removeLayer(poligoni[j]);
                }
                for(var i = 3; i < bolnisnice.features.length; i++){
                  if(bolnisnice.features[i].geometry.type == "Polygon"){
                    for(var j in bolnisnice.features[i].geometry.coordinates[0]){
                      bolnisnice.features[i].geometry.coordinates[0][j].reverse();
                    }
                    var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates);
                     polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                     " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                     polygon.addTo(mapa);
                     poligoni.push(polygon);
                  }
                }
                for(var i = 0; i < 3; i++){
                  if(bolnisnice.features[i].geometry.type == "Polygon"){
                    for(var j in bolnisnice.features[i].geometry.coordinates){
                      for(var k in bolnisnice.features[i].geometry.coordinates[j]){
                        bolnisnice.features[i].geometry.coordinates[j][k].reverse();
                      }
                    }
                    
                    if(i == minIndex){
                      console.log("kao zeleni multipolygon");
                      
                       var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates, {color: 'green'});
                       polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                       " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                       polygon.addTo(mapa);
                       poligoni.push(polygon);
                      
                      
                      
                    }else{
                      var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates);
                       polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                       " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                       polygon.addTo(mapa);
                       poligoni.push(polygon);
                    }
                  }
                }
                
                
                
                
              }else{//navaden polygon
                for(var j in poligoni){
                  mapa.removeLayer(poligoni[j]);
                  //console.log("removed one!");
                }
                for(var j in multipoligoni){
                  for(var k in multipoligoni[j]){
                    mapa.removeLayer(multipoligoni[j][k]);
                    //console.log("removed a multipoly");
                  }
                }
                for(var i = 0; i < 3; i++){
                  if(bolnisnice.features[i].geometry.type == "Polygon"){
                    for(var j in bolnisnice.features[i].geometry.coordinates){
                      for(var k in bolnisnice.features[i].geometry.coordinates[j]){
                        bolnisnice.features[i].geometry.coordinates[j][k].reverse();
                      }
                    }
                    var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates);
                     polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                     " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                     polygon.addTo(mapa);
                     poligoni.push(polygon);
                  }
                }
                for(var i = 3; i < bolnisnice.features.length; i++){
                  if(bolnisnice.features[i].geometry.type == "Polygon"){
                    for(var j in bolnisnice.features[i].geometry.coordinates[0]){
                      bolnisnice.features[i].geometry.coordinates[0][j].reverse();
                    }
                    
                    if(i == minIndex){
                      console.log("kao zeleni polygon");
                      
                       var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates, {color: 'green'});
                       polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                       " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                       polygon.addTo(mapa);
                       poligoni.push(polygon);
                      
                      
                      
                    }else{
                      var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates);
                       polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
                       " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
                       polygon.addTo(mapa);
                       poligoni.push(polygon);
                    }
                  }
                }
              }
              break;
            case "LineString":
              break;  
            }
          
      });
    }
function dodajBolnisniceNaZemljevid(){
  pridobiPodatke(function(bolnisnice){
    for(var i in bolnisnice.features){
      var koord = [];
      if (bolnisnice.features[i].geometry.type == "Polygon"){//if Polygon
        if(bolnisnice.features[i].geometry.coordinates.length == 1){//if navaden Polygon
          for(var j in bolnisnice.features[i].geometry.coordinates[0]){
            bolnisnice.features[i].geometry.coordinates[0][j].reverse();
            koord.push(bolnisnice.features[i].geometry.coordinates[0][j]);
          }
          var polygon = L.polygon(bolnisnice.features[i].geometry.coordinates);
          polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
          " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
          polygon.addTo(mapa);
          poligoni.push(polygon);
          koordinate.push([koord, i]);
        }else if(bolnisnice.features[i].geometry.coordinates.length > 1){              //če multipolygon
          var skupina = [];
          for(var j in bolnisnice.features[i].geometry.coordinates){ //za vsak polygon
            polygon = null;
            for(var k in bolnisnice.features[i].geometry.coordinates[j]){  //za vsak par koordinat
              bolnisnice.features[i].geometry.coordinates[j][k].reverse();
              koord.push(bolnisnice.features[i].geometry.coordinates[j][k]);
            }
            polygon = L.polygon(bolnisnice.features[i].geometry.coordinates[j]/*, {color: 'black'}*/);
            polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
            " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
            polygon.addTo(mapa);
            skupina.push(polygon);
            //console.log("Dodali smo del multipolyja");
            koordinate.push([koord, i]);
          }
          multipoligoni.push(skupina);
        }
      }else if(bolnisnice.features[i].geometry.type == "Point"){//else if Point
        var marker = L.marker([bolnisnice.features[i].geometry.coordinates[1], bolnisnice.features[i].geometry.coordinates[0]]);
        koord.push([bolnisnice.features[i].geometry.coordinates[1], bolnisnice.features[i].geometry.coordinates[0]]);
        marker.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] + " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
        marker.addTo(mapa);
        markerji.push(marker);
        koordinate.push([koord, i]);
      }else{ //linestring geometry type???
      koord = [];
        for(var j in bolnisnice.features[i].geometry.coordinates[0]){//za vsak par koordinat
          bolnisnice.features[i].geometry.coordinates[j].reverse();
          koord.push(bolnisnice.features[i].geometry.coordinates[j]);
        }
        var polygon = L.geoJSON({
            "type": "LineString",
            "coordinates": koord
        });
        polygon.bindPopup(bolnisnice.features[i].properties.name + "<br>" + bolnisnice.features[i].properties["addr:city"] +
        " " + bolnisnice.features[i].properties["addr:street"] + " " + bolnisnice.features[i].properties["addr:housenumber"]);
        polygon.addTo(mapa);
        lineStringi.push(polygon);
        koordinate.push([koord, i]);
      }
    }
    //console.log(koordinate);
  });
}
function pridobiPodatke(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "knjiznice/json/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        callback(json);
    }
  };
  xobj.send(null);
}