/* global $ */
/* global btoa*/

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


	

function generirajPodatke() {
  $("#preberiObstojeciEHR").html("<option value=" + '"' + '"' + "></option>");
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Donald",
          lastNames: "Drunk",
          dateOfBirth: "1988-12-12",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            
          		var podatki = [{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1234-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "123",
          		    "vital_signs/body_weight/any_event/body_weight": "123",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "34",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "90",
          		    "vital_signs/blood_pressure/any_event/diastolic": "123",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "98",
          		    "vital_signs/pulse/any_event/rate":"64"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1345-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "134",
          		    "vital_signs/body_weight/any_event/body_weight": "134",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "45",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "123",
          		    "vital_signs/blood_pressure/any_event/diastolic": "156",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "88",
          		    "vital_signs/pulse/any_event/rate":"66"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1456-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "145",
          		    "vital_signs/body_weight/any_event/body_weight": "145",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "44",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "90",
          		    "vital_signs/blood_pressure/any_event/diastolic": "156",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "43",
          		    "vital_signs/pulse/any_event/rate":"123"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1567-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "167",
          		    "vital_signs/body_weight/any_event/body_weight": "122",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "37",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "123",
          		    "vital_signs/blood_pressure/any_event/diastolic": "180",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "78",
          		    "vital_signs/pulse/any_event/rate":"104"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2003-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "233",
          		    "vital_signs/body_weight/any_event/body_weight": "165",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "78",
          		    "vital_signs/blood_pressure/any_event/diastolic": "98",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "99",
          		    "vital_signs/pulse/any_event/rate":"54"
          		}];
          		for(var i = 0; i < 5; i++){
          		  var parametriZahteve = {
          		    ehrId: ehrId,
          		    templateId: 'Vital Signs',
          		    format: 'FLAT',
          		    committer: "Zofka Zofka"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(parametriZahteve),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(podatki[i]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
                  success: function () {
                    console.log("#1success");
                  },
                  error: function() {
                  	console.log("#1notSuccess");
                  }
            		});
          		}
          	$("#preberiObstojeciEHR").append("<option value=" + '"' + ehrId + '"' + ">Donald Drunk</option>");
          	$("#ehrID1").html("<span>Donald Drunk: " + ehrId + "<br>");
          },
          error: function(err) {
          	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
  
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Donald",
          lastNames: "Drumpf",
          dateOfBirth: "1900-01-01",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            var podatki = [{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2000-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "123",
          		    "vital_signs/body_weight/any_event/body_weight": "45",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "90",
          		    "vital_signs/blood_pressure/any_event/diastolic": "123",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "98",
          		    "vital_signs/pulse/any_event/rate":"74"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2001-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "134",
          		    "vital_signs/body_weight/any_event/body_weight": "48",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "40",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "67",
          		    "vital_signs/blood_pressure/any_event/diastolic": "78",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "88",
          		    "vital_signs/pulse/any_event/rate":"133"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2005-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "166",
          		    "vital_signs/body_weight/any_event/body_weight": "55",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "38",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "78",
          		    "vital_signs/blood_pressure/any_event/diastolic": "120",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "98",
          		    "vital_signs/pulse/any_event/rate":"111"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2009-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "167",
          		    "vital_signs/body_weight/any_event/body_weight": "59",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "37",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "54",
          		    "vital_signs/blood_pressure/any_event/diastolic": "110",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "67",
          		    "vital_signs/pulse/any_event/rate":"104"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2012-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "170",
          		    "vital_signs/body_weight/any_event/body_weight": "65",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "78",
          		    "vital_signs/blood_pressure/any_event/diastolic": "98",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "99",
          		    "vital_signs/pulse/any_event/rate":"87"
          		}];
          		for(var i = 0; i < 5; i++){
          		  var parametriZahteve = {
          		    ehrId: ehrId,
          		    templateId: 'Vital Signs',
          		    format: 'FLAT',
          		    committer: "Zofko Zofkic"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(parametriZahteve),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(podatki[i]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
                  success: function () {
                    console.log("#2success");
                  },
                  error: function() {
                  	console.log("#2notSuccess");
                  }
            		});
          		}
            $("#preberiObstojeciEHR").append("<option value=" + '"' + ehrId + '"' + ">Donald Drumpf</option>");
            $("#ehrID2").html("<span>Donald Drumpf: " + ehrId + "<br>");
            
          },
          error: function(err) {
          	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
  
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: "Ronald",
          lastNames: "Dank",
          dateOfBirth: "2001-10-13",
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            //TODO DODAJ V DROPDOWN MENU
            var podatki = [{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1234-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "189",
          		    "vital_signs/body_weight/any_event/body_weight": "102",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "100",
          		    "vital_signs/blood_pressure/any_event/diastolic": "100",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "99",
          		    "vital_signs/pulse/any_event/rate":"45"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1666-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "189",
          		    "vital_signs/body_weight/any_event/body_weight": "78",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "38",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "100",
          		    "vital_signs/blood_pressure/any_event/diastolic": "100",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "98",
          		    "vital_signs/pulse/any_event/rate":"66"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1988-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "189",
          		    "vital_signs/body_weight/any_event/body_weight": "123",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "34",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "65",
          		    "vital_signs/blood_pressure/any_event/diastolic": "90",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "99",
          		    "vital_signs/pulse/any_event/rate":"65"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "1999-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "198",
          		    "vital_signs/body_weight/any_event/body_weight": "122",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "37",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "100",
          		    "vital_signs/blood_pressure/any_event/diastolic": "100",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "78",
          		    "vital_signs/pulse/any_event/rate":"104"
          		},{
          		    "ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": "2003-12-12T12:12Z",
          		    "vital_signs/height_length/any_event/body_height_length": "233",
          		    "vital_signs/body_weight/any_event/body_weight": "144",
          		   	"vital_signs/body_temperature/any_event/temperature|magnitude": "36",
          		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
          		    "vital_signs/blood_pressure/any_event/systolic": "100",
          		    "vital_signs/blood_pressure/any_event/diastolic": "100",
          		    "vital_signs/indirect_oximetry:0/spo2|numerator": "99",
          		    "vital_signs/pulse/any_event/rate":"54"
          		}];
          		for(var i = 0; i < 5; i++){
          		  var parametriZahteve = {
          		    ehrId: ehrId,
          		    templateId: 'Vital Signs',
          		    format: 'FLAT',
          		    committer: "Zofka Zofka"
            		};
            		$.ajax({
                  url: baseUrl + "/composition?" + $.param(parametriZahteve),
                  type: 'POST',
                  contentType: 'application/json',
                  data: JSON.stringify(podatki[i]),
                  headers: {
                    "Authorization": getAuthorization()
                  },
                  success: function () {
                    console.log("#3success");
                  },
                  error: function() {
                  	console.log("#3notSuccess");
                  }
            		});
          		}
            $("#preberiObstojeciEHR").append("<option value=" + '"' + ehrId + '"' + ">Ronald Dank</option>");
            $("#ehrID3").html("<span>Ronald Dank: " + ehrId + "<br>");
            
          },
          error: function(err) {
          	$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
		});
}

 
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosimo, vnesite pravilen ehrID!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiMeritveVitalnihZnakovSporocilo").html("<div>Bolnik " + party.firstNames + " " + //TODO TUKAJ HTML ZA IZPIS
          party.lastNames + ": Rojen " + party.dateOfBirth +
          ".</div>");
  		},
  		error: function() {
  			$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Prosimo vnesite pravilen ehrID!");
  		}
		});
	}
}

function chuckNorrisFact(){
  
/* global Plotly */

  $.ajax({
    url: "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random",
    type: 'GET',
    headers: {
      "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com",
      "X-RapidAPI-Key": "0822e15d7bmsh2330b866c951103p19996djsne1f9cc77eed7",
      "accept": "application/json"
    },
    success: function (result) {
      //console.log(result);
      $("#chuckNorrisFact").html(
        "<span>" + result.value);
    },
    error: function() {
    	$("#chuckNorrisFact").html(
        "<span>Napaka: Chuck Norris se je spotaknil čez povezavo!");
    }
  });
}
function preberiMeritveVitalnihZnakov() {
	var ehrId = $("#preberiEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();
  var tipEnglish = "";
  
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosimo, vnesite pravilen ehrID!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  				var party = data.party;
  				$("#preberiMeritveVitalnihZnakovSporocilo").html("<br/><span>Prikaz podatka <b>" + '"' + tip + '"' + "</b> za bolnika <b>" + '"' + party.firstNames +
            " " + party.lastNames + '"' + "</b>.</span><br/><br/>");
          tipEnglish = "";
          switch(tip){
            case "Višina":
              tipEnglish = "height";
              break;
            case "Teža":
              tipEnglish = "weight";
              break;
            case "Temperatura":
              tipEnglish = "body_temperature";
              break;
            case "Krvni tlak":
              tipEnglish = "blood_pressure";
              break;
            case "Nasičenost krvi s kisikom":
              tipEnglish = "spO2";
              break;
            case "Pulz":
              tipEnglish = "pulse";
              break;
            default:
              tipEnglish = "";
              break;
          }
          $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + tipEnglish,
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>" + tip + "</th></tr>";
                    //var podatek = [];
                    var arrayVrednosti = [];
    				        var arrayMeritev = [];
    				        var casMeritev = [];
    				        var count = 1;
    				        var trace1;
    				        var data;
                    var layout;
                    switch(tipEnglish){
                      case "height":
                        
                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].height +
                            " " + res[i].unit + "</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].height);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip,
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        data = [trace1];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka "' + tip + '" skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);
                        break;
                        
                      case "weight":
                        
                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].weight +
                            " " + res[i].unit + "</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].weight);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip,
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        data = [trace1];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka "' + tip + '" skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);
                        break;
                        
                      case "body_temperature":
                        
                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].temperature +
                            " " + res[i].unit + "</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].temperature);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip,
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        data = [trace1];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka "' + tip + '" skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);
                        break;
                        
                      case "blood_pressure":
                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].diastolic + "/" + res[i].systolic + 
                            " " + res[i].unit + "</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        var arrayVrednosti2 = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].diastolic);
        				          arrayVrednosti2.push(res[i].systolic);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip + ', Diastolični',
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        var trace2 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti2,
                          mode: 'lines+markers',
                          name: tip + ', Sistolični',
                          text: casMeritev,
                          line: {
                            color: 'rgb(255, 0, 30)',
                            width: 3
                          }
                        };
                        data = [trace1, trace2];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka ' + tip + ' skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);
                        break;

                      case "spO2":

                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].spO2 + "%</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].spO2);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip,
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        data = [trace1];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka "' + tip + '" skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);

                        break;

                      case "pulse":
                        
                        for (var i in res) {
          		            results += "<tr><td>" + res[i].time +
                            "</td><td class='text-right'>" + res[i].pulse +
                            " " + res[i].unit + "</td></tr>";
        				        }
        				        arrayVrednosti = [];
        				        arrayMeritev = [];
        				        casMeritev = [];
        				        //name = tip
        				        count = 1;
        				        for (var i in res){
        				          arrayVrednosti.push(res[i].pulse);
        				          arrayMeritev.push(count);
        				          casMeritev.push(res[i].time);
        				          count++;
        				        }
        				        trace1 = {
                          x: arrayMeritev.reverse(),
                          y: arrayVrednosti,
                          mode: 'lines+markers',
                          name: tip,
                          text: casMeritev,
                          line: {
                            color: 'rgb(30, 0, 255)',
                            width: 3
                          }
                        };
                        data = [trace1];
                        layout = {
                          height: 500,
                          width: 500,
                          title: 'Spreminjanje podatka "' + tip + '" skozi čas',
                          xaxis: {
                            range: [0.75, (count + 0.25)],
                            title: 'Meritve'
                          },
                          yaxis: {
                            range: [0.0, Math.max(arrayVrednosti)],
                            title: tip,
                            zeroline: true
                          }
                        };
                        Plotly.newPlot('grafiTu', data, layout);
                        break;
                      default:
                        break;
                    }
                    
    				        
                    results += "</table>";
        				    $("#preberiMeritveVitalnihZnakovSporocilo").append(results);
  				        
    			    	} else {
    			    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Prosimo vnesite pravilen ehrID!");
    			    }
  					});
  				},
  				error: function() {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Prosimo vnesite pravilen ehrID!");
    			    }
		});
	}
}
$(document).ready(function() {
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiEHRid").val($(this).val());
	});
	$("#preberiTipZaVitalneZnake").change(function(){
	  preberiMeritveVitalnihZnakov();
	})
});